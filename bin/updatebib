#!/usr/bin/env python
import sys
import os

import subprocess

  
from updatebib import ProjectManager
from updatebib import UpdatebibError
  
import updatebib  
  
def print_help(short):
  print "Usage: updatebib [switches] | [projects]"
  
  if short: return
  
  print ""
  print "A program that is designed to synchronize bibtex files"
  print ""
  print "v" + updatebib.__version__
  print ""
  print "[projects] "
  print "A list of projects to update, if no arguments is given the updatebib will "
  print "update the project with the same name as the current folder."
  print ""
  print "Switches:"
  print "      --updatebib-file FILE     Specifies the file that contains our porjects."
  print "      --all                     Updates every project."
  print "  -e, --edit OPTIONAL EDITOR    Opens the config file in a selected editor The "
  print "                                default editor can be specified the"
  print "                                UPDATEBIB_EDITOR environment variable."
  print "  -d, --diff                    Shows a diff using the between the selected "
  print "                                projects target and source files using the"
  print "                                difftool specified in UPDATEBIB_DIFFTOOL"
  print "  -l, --loaded                  Shows the config file, use --loaded for verbose."
  print "  -a, --add NAME TARGET SOURCE  Adds a project to the our settings file"
  print "  -r, --remove NAME(S)          removes the specified projects."
  print "  -h, --help                    Prints this help message"
  print ""
  print "Default values can be placed in the UPDATEBIB_OPTS environment variable"
  
def add_reference_file(references_file):
  name = sys.argv[2]
  target = sys.argv[3]
  source = sys.argv[4]

  project = ProjectManager(references_file)
  project.add(name, target, source)

def edit(file, editor):

  if editor == None:
    editor = os.getenv("UPDATEBIB_EDITOR")

    if editor == None:
      raise UpdatebibError("No editor specified")
      return

  subprocess.call([editor, file])  

def get_opt_args():
  args = os.getenv("UPDATEBIB_OPTS")
  if args == None or not any(args):
    return []
  else:
    return args.split(" ")

def main():
  
  references_file = os.path.join(os.getenv('HOME'), ".updatebib")
  args = get_opt_args() + sys.argv[1:]

  diff_instead = False
  diff_tool = None
  action = None
  
  if len(args) > 1 and args[0] == "--updatebib-file":
    args = args[1:]

    # The first argument is the file to our settings, take it and use the tail
    # as the projects to update. If we don't have any files/projects at all
    # we treat it as trying to update current dir without any settings file.
    if any(args):
      references_file = args[0]
      args = args[1:]
    else:
      references_file = ""
  
  
  if any(args):
    action = args[0]
    args = args[1:]
  
  manager = ProjectManager(references_file)
  
  if action == '-h':
    print_help(True)
    return 0
  elif action == '--help':
    print_help(False)
    return 0
  elif action == '-l':
    manager.show_entries(False)
    return 0
  elif action == '--loaded':
    manager.show_entries(True)
    return 0
  elif action in ['-e', '--edit']:
    editor = None
    if any(args):
      editor = args[1]
    edit(references_file, editor)
    return 0
  elif action in ['-a', '--add']:
    if not len(args) == 3:
      raise UpdatebibError("usage: updatebib " + action + " NAME TARGET SOURCE")
    else:
      add_reference_file(references_file)
    return 0
  elif action in ['-r', '--remove']:
    if not any(args):
      raise UpdatebibError("usage: updatebib " + action + " NAME(S)")
    else:
      manager.remove_projects(args)
    return 0
  elif action in ['-d', '--diff']:
    diff_instead = True

  if action == "--all":
    reference_files = manager.all_projects()
  else:
    if action != None:
      args.append(action)
    reference_files = manager.projects(args)

  if diff_instead and diff_tool == None:
    diff_tool = os.getenv('UPDATEBIB_DIFFTOOL')

    if diff_tool == None:
      print "No diff tool selected"
      return 1 

  for reference_file in reference_files:
    if not diff_instead:
      reference_file.update()
    else:
      reference_file.diff(diff_tool)
  
  return 0
    
if __name__ == "__main__":
  try:
    sys.exit(main())    
  except UpdatebibError as e:
    print e.value
    sys.exit(1)
