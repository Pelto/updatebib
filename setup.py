#!/usr/bin/python

from distutils.core import setup

import updatebib

setup(
    name='updatebib',
    version=updatebib.__version__,
    description='A program that is designed to synchronize bibtex files',
    author='Johannes Pelto-Piri',
    author_email='johannes.peltopiri@gmail.com',
    url='http://www.bitbucket.org/Pelto/updatebib',
    scripts=['bin/updatebib'],
    packages=[
      'bibtex',
      'updatebib'
    ])