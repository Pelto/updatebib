import sys
import os

from entry import Entry
from projectloader import ProjectLoader
from updatebiberror import UpdatebibError

class ProjectManager():
  
  """The project manager in updatebib"""
  def __init__(self, path):
    self.entries = []
    self.path = path
    self.loaded = False
  
  def load(self):
    loader = ProjectLoader()
    entries, errors = loader.load_updatebib_file(self.path)
    
    if any(errors):
      messages = reduce(lambda s1, s2: s1 + s2, map(lambda: error, " -" + error + "\n", errors))
      error_message = "There were errors while reading " + file_path + "\n" + messages
        
      raise UpdatebibError(error_message)
    
    self.entries = entries
    self.loaded = True
    
  def all_projects(self):   
    if not self.loaded: self.load()
    return self.entries
    
  def projects(self, names):
    if len(names) == 0:
      path, dir_name = os.path.split(os.getcwd())
      names = [dir_name]
    
    reference_files = self.all_projects()
              
    if not any(reference_files):
      raise UpdatebibError("Please configure your `" + references_file + "` before continuing")
    
    return self.reference_files_to_update(reference_files, names)
    
  def reference_files_to_update(self, reference_files, names):
    
    selected_references = []

    for name in names:
      matching = filter(lambda reference_file: reference_file.name == name, reference_files)

      if not any(matching):
        raise UpdatebibError("Could not find a reference file for " + name)
      elif len(matching) > 1:
        raise UpdatebibError("Multiple entries for " + name)
      else:
        selected_references.append(matching[0])

    return selected_references
  
  def show_entries(self, verbose):
    if not self.loaded: self.load()
    if not any(self.entries):
      raise UpdatebibError(self.path + " is not yet configured")
    else: 
      for entry in self.entries:
        if verbose:
          print entry
          print ""
        else:
          print entry.name
  
  def add(self, name, target, source):

    # For this file we use the current directory
    # as the default source directory.
    entry = Entry(name, source, target, [], os.getcwd(), os.getcwd())

    if not os.path.exists(source):
      raise UpdatebibError("The source file does not exists")
    elif not os.path.exists(target):
      raise UpadtebibError("The target file does not exists")
    else:
      with open(self.path, "a") as f:
        f.write("\n")
        f.write(str(entry))

        print "Saved " + entry.name
  
  def remove_projects(self, projects):
    self.load()
    
    remove_entry = lambda entry: not entry.name in projects
    entries_to_keep = filter(remove_entry, self.entries)

    with open(self.path, "w") as f:
      f.truncate()
      for entry in entries_to_keep:
        f.write("\n")
        f.write(str(entry))