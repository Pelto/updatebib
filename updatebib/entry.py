import os

from bibtex import bibparse

from updatebiberror import UpdatebibError

class Entry():
  
  def __init__(self, name, source, target, fields_to_skip, default_source, default_target):
    
    def fix_path(path, default):
      if path.startswith("/") or path.startswith("~/"):
        return path
      return os.path.join(default, path)
    
    self.name = name  
      
    self.source = fix_path(source, default_source)
    self.target = fix_path(target, default_target)
    self.fields_to_skip = fields_to_skip
    
    if not os.path.exists(self.source):
      message = "The target file " + self.source + " for project " + self.name + " does not exists"
      raise UpdatebibError(message)
      
  def valid(self):
    return os.path.exists(self.source)
    
  def update(self):     
    # The bibtex entries from the source file.
    bibtex_entries = bibparse.parse_bib(self.source)

    # Remove any fields that we
    # are to skip from the source.
    for entry in bibtex_entries:
      for field_to_skip in self.fields_to_skip:
        if field_to_skip in entry.data:
          del entry.data[field_to_skip]  

    # Remove the file if it already exists, 
    # easier to just create a new file. 
    if os.path.exists(self.target): 
      os.remove(self.target)

    with open(self.target, "wc") as f:
      for entry in bibtex_entries: 
        f.write(str(entry))

    print "Updated " + self.name

  def diff(self, diff_tool):
    args_tool = diff_tool.split(" ")
    args = args_tool + [self.target, self.source]
    subprocess.Popen(args)
    
  def __str__(self):
    lines = [] 
    lines.append(":entry")
    lines.append("\t:name " + self.name)
    lines.append("\t:target " + self.target)
    lines.append("\t:source " + self.source)
    
    if any(self.fields_to_skip):
      lines.append("\t:skip-fields " + reduce(lambda field1, field2: field1 + " "+ field2, self.fields_to_skip))
    
    return reduce(lambda line1, line2: line1 + "\n" + line2, lines)