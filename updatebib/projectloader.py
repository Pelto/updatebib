import os
from entry import Entry

class ProjectLoader():
  """The loader for the projects."""
  def __init__(self):
    self.entries = []
    self.errors = []
    
  def read_lines(self, path):
    lines = []
    with open(path) as source:
      lines = source.readlines()
    return lines
    
  """Concats several strings into one"""
  def concat_parts(self, parts, start):
    return reduce(lambda part1, part2: part1 + " " + part2, parts[start:]).strip()

  """Adds an entry to the collection, or the error if the entry is invalid."""
  def add_entry(self, entry_name, entry_source, entry_target, entry_fields_to_skip, default_source, default_target):
    entry = Entry(entry_name, entry_source, entry_target, entry_fields_to_skip, default_source, default_target)

    if not entry.valid():
      self.errors.append(entry_name + " can't be loaded")
    else:
      self.entries.append(entry)

  def load_updatebib_file(self, references_file):
    
    self.entries = []
    self.errors = []
    
    if not os.path.isfile(references_file):
      return self.entries, self.errors
      
    settings_lines = self.read_lines(references_file)

    default_source = ""
    default_target = ""

    in_entry = False

    entry_name = ""
    entry_target = ""
    entry_name = ""
    entry_fields_to_skip = []

    for line in map(lambda line: line.strip(), settings_lines):

       if line.startswith("#"): continue

       parts = line.split(" ")

       if any(parts):
         if parts[0] == ":default-source": 
           default_source = self.concat_parts(parts, 1)
         if parts[0] == ":default-target": 
           default_target = self.concat_parts(parts, 1)

         if parts[0] == ":entry":
           if in_entry: self.add_entry(entry_name, entry_source, entry_target, entry_fields_to_skip, default_source, default_target)
           in_entry = True

           entry_name = ""
           entry_target = ""
           entry_name = ""
           entry_fields_to_skip = []

         if parts[0] == ":name": entry_name = self.concat_parts(parts, 1)
         if parts[0] == ":source": entry_source = self.concat_parts(parts, 1)
         if parts[0] == ":target": entry_target = self.concat_parts(parts, 1)
         if parts[0] == ":skip-fields": entry_fields_to_skip = map(lambda field: field.lower(), parts[1:])
         
    # Add any non-added entry. 
    if in_entry: self.add_entry(entry_name, entry_source, entry_target, entry_fields_to_skip, default_source, default_target)

    return self.entries, self.errors
  